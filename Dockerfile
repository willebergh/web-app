FROM node

WORKDIR /app

COPY ./build .

EXPOSE 8080

CMD ["node", "app.js"]